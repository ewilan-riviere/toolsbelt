<?php

namespace App\Http\Requests;

class UpdateSubmissionRequest extends StoreSubmissionRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        parent::rules();
    }
}
